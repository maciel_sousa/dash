FROM node

WORKDIR /usr/app

COPY package.json ./

RUN npm install -g @prisma/client
RUN npm install

COPY . .

EXPOSE 4000

CMD ["npm", "run", "dev"]